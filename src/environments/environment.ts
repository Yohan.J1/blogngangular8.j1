// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDcc9B4RI1Pokp9mQJqmTtfGCHBnEhXQLs",
    authDomain: "ngblogj1.firebaseapp.com",
    databaseURL: "https://ngblogj1.firebaseio.com",
    projectId: "ngblogj1",
    storageBucket: "ngblogj1.appspot.com",
    messagingSenderId: "729000193126",
    appId: "1:729000193126:web:b50b973116d51c784a91a7",
    measurementId: "G-DEGVYDWVW4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule,
		MatButtonModule,
		MatMenuModule,
		MatToolbarModule,
		MatIconModule,
		MatSidenavModule,
		MatListModule,
		MatProgressSpinnerModule,
		MatDividerModule,
		MatChipsModule,
		MatFormFieldModule,
		MatInputModule,
		MatTableModule,
		MatPaginatorModule,
		MatSortModule,
		MatDialogModule,
		MatSelectModule
	} from '@angular/material';

const myModules = [ MatCardModule,
	MatButtonModule,
	MatMenuModule,
	MatToolbarModule,
	MatIconModule,
	MatSidenavModule,
	MatListModule,
	MatProgressSpinnerModule,
	MatDividerModule,
	MatChipsModule,
	MatFormFieldModule,
	MatInputModule,
	MatTableModule,
	MatPaginatorModule,
	MatSortModule,
	MatDialogModule,
	MatSelectModule
];

@NgModule({
  declarations: [],
  imports: [ CommonModule, myModules ],
  exports: [ myModules ]
})
export class MaterialModule { }

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { PostService } from 'src/app/components/posts/post.service';
import { PostI } from '../../models/post.interface';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './../modal/modal.component';

export interface PeriodicElement {
	name: string;
	position: number;
	weight: number;
	symbol: string;
}

@Component({
	selector: 'app-table',
	templateUrl: './table.component.html',
	styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {

	constructor(private postSvc: PostService, public dialog: MatDialog) { }
	displayedColumns: string[] = ['titlePost', 'tagsPost', 'actions'];
	dataSource = new MatTableDataSource();
	@ViewChild(MatPaginator, { static: true})paginator: MatPaginator;
	@ViewChild(MatSort, {static: true})sort: MatSort;

	ngOnInit() {
		this.postSvc.getAllPost()
			.subscribe(posts => 
				this.dataSource.data = posts);
	}

	ngAfterViewInit(){
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	applyFilter(filterValue: string) {
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}

	onEditPost(post: PostI){
		this.openDialog(post);
	}

	onDeletePost(post: PostI){
		console.log('DeletePost =>', post);
		Swal.fire({
			title:`Are you sure?`,
			text: `You won't be able to revert this!`,
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.value){
				this.postSvc.deletePostById(post).then(() => {
					Swal.fire('Deleted!', 'Your file has been deleted.', 'success')
				}).catch((error) => {
					Swal.fire('Error', 'There was a error deleting this posts', 'error')
				})
			} else if ( result.dismiss === Swal.DismissReason.cancel) 
				{ Swal.fire('Cancelled', 'Your file is safe :)', 'error') }
			}
		)
	}

	onNewPost(){
		this.openDialog();
	}

	openDialog(post?: PostI):void{
		const config = {
			data: {
				message : post ? 'Edit Post' : 'New Post',
				content: post
			}
		};
		const dialogRef = this.dialog.open(ModalComponent, config);
		dialogRef.afterClosed().subscribe(res => {
			console.log(`res => ${res}`);
		})
	}
}
